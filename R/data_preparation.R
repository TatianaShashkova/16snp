# data preparation

#' read raw VCF tables for future analysis
#' @param vcf_paths named vector of paths to VCFs files, names - sample-IDs
#' @return named list of data tables, where each table represents one sample
#' @import data.table
#' VCF tables MUST contain information about alternative alleles depth, run of samtools mpileup with "-t  INFO/AD" key

get_VCFs <- function(vcf_paths, sample_ids) {

  VCFs <- lapply(vcf_paths, function(path) {
    temp <- fread(path, header = T, stringsAsFactors = F, skip = "CHROM")
    colnames(temp)[1] = "OTU"
    return(temp)
  })

  VCFs <- lapply( VCFs, function(df) df[,c("OTU","POS","REF","ALT","INFO")] )

  #adds a colon of alt allele depth for VCF file
  ALT1 <- function(df) {
    alt1 <- function(string){
      string <- as.character(string)
      AD <- strsplit(string, ';')[[1]][2] #'AD=62,15'
      alt1 <- strsplit(AD, ',')[[1]][2]
      return(alt1)
    }
    alt <- sapply(df$INFO, alt1 )
    df$ALT1 <- as.integer(alt)
    return(df)
  }

  # adds a  col of alt2 depth, if no - NA
  ALT2 <- function(df) {
    alt2 <- function(string){
      string <- as.character(string)
      AD <- strsplit(string, ';')[[1]][2] #'AD=4,5,4'
      alt2 <- strsplit(AD, ',')[[1]][3]
    }
    alt <- sapply(df$INFO,alt2)
    df$ALT2 <- as.integer(alt)
    return(df)
  }

  VCFs <- lapply(VCFs, ALT1)
  VCFs <- lapply(VCFs, ALT2)

  return(VCFs)
}

#' read raw coverage tables from bedtools for future analysis
#' @param cov_paths named vector of paths to Coverage files, names - sample-IDs
#' @return named list of named vectors, where each vector represents one sample,
#' each element of vector represents one OTU and it's coverage in sample
#' @import data.table

get_coverage <- function(cov_paths){
  coverage <- lapply(cov_paths, function(path)
    fread(path, header = F, stringsAsFactors = T))

  coverage_ref <- lapply(coverage, function(df) {
    df <- df[,c("V1","V3")]
    colnames(df)<- c("OTU", "DP")
    temp = df[, trunc(mean(DP)), by = OTU]
    DP = temp$V1
    names(DP) <- temp$OTU
    return(DP)
  })

  return(coverage_ref)
}
